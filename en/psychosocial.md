---
description: Caring for the wellbeing of yourself, organizations, and movements
---

# ❤️ Psychosocial Support



<figure><img src=".gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

If you are a mental health practitioner, defender, student, makiisa and join our mailing list to ensure you receive our invitations for free upcoming workshops and dialogues this 2024: [https://bit.ly/MentalHealthSocialJustice](https://bit.ly/MentalHealthSocialJustice?fbclid=IwAR1KkzyKqKRpL9IjLn3FiZ76WBqqiTrx\_NRQJs73Qizmz2e7DePn1lr6oCI\_aem\_AcYatuCGmnPvw45aa\_9QXbMbdKrVPSssGSlC-12U62TTDoBRTrK6Y\_Jb58rNcNjq5lhGNo2AYrTiC-17eexCMH1U)

Every single workshop/dialogue will have a social justice defender or organization, sharing stories of lived experiences.

Sign up if you're curious about:

* Mental health workshops exploring colonization and narcoculture by ecofeminist, queer affirmative, Colombian, Sudanese, maybe Muslim practitioners?
* Filipino folk psychology too?
* Workshops on peer counselling and supporting loved ones and friends affected by enforced disappearances, red-tagging, and more?
* Connections of inflation, food sovereignty, migration, labor rights, transportation, veganism, militarization, climate, to mental health?
