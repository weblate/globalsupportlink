---
description: How to stay safe where you're staying
---

# Hotel Security

It is important to ensure you feel safe in the place that you’ll be staying. Take the time to conduct the following security assessment upon arrival in a new place:&#x20;

* Check the room’s access points (doors, windows, fire exits).
* Examine the room, including cabinets, bathrooms, beds, and window areas for anything that appears suspicious.
* Make sure the telephone is working properly by calling the front desk.
* Make sure colleagues have the hotel location, room number and telephone number.
* Note the evacuation route in case of fire or emergency, and use the stairways at least once to become familiar with them.\


Keep in mind the following best practices when staying in a hotel:

* Always secure doors when inside the room with locks and security chains.
* Keep room curtains closed when it is dark outside.
* Don’t open the door to visitors (including hotel staff) unless it is possible to positively identify them. Use the peephole or call the front desk for verification.
* If available, use the hotel’s safe deposit boxes to store cash, credit cards, passport, and any other valuables. Don’t leave valuables or sensitive documents in the room.
* Carry a copy of your passport and visa and leave the originals in the hotel or the office. Additionally, keep a copy of your passport back home.\


### Hotel Safety & Security Assessment

Below is a checklist that can help you remember what to look for when checking into a new hotel or evaluating one prior to reserving a room. While not all items on this list are always necessary or available, it is important to try to check as many boxes as possible. It could be a red flag if you notice that the hotel has very few protective security measures in place, and therefore important to address with your colleagues or line manager.&#x20;

Fire Safety

☐ Fire emergency plan/map

☐ Smoke detectors

☐ Sprinklers

☐ Emergency exit doors

☐ Fire alarm system

☐ Fire extinguishers

☐ Emergency stairwells

☐ Multiple exits/entrances\


Security systems and equipment

☐ CCTV surveillance system

☐ Controlled parking

☐ Key/badge controlled access



Environment

☐ Major road access

☐ Barriers/fencing/gates

☐ Sufficient standoff distance (from perimeter to hotel)

☐ No nearby government/military buildings

☐ Low crime area



Lighting

☐ Emergency lighting in public areas and evacuation stairwells

☐ Lighted parking areas

☐ Lighted premises and grounds\


Profile

☐ Low profile hotel, unlikely to be targeted

☐ Not heavily frequented by international travellers, e.g. NGO workers, tourists, etc. (In some areas, higher-end, Western-chain hotels with many tourists or foreigners present could be targeted by extremist groups. However, these hotels often have very good protective security measures. This is one of the reasons why it is so important to conduct a Rapid Risk Analysis prior to travel or booking a hotel. It is vital to weigh the risks based on the context.)\


Guarding

☐ Onsite security staff 24 hours/day

☐ Security patrols

☐ Supervised access to public entrances/exits

☐ Staffed command centre

☐ Armed guards (only advisable in certain contexts, otherwise it may draw more attention and create more risk for the traveller)

Health Safety

☐ First aid/trauma kits

☐ AED equipment\


Guest Room&#x20;

☐ Deadbolt locks

☐ Door chain/wishbone latch

☐ Peepholes

☐ Safety exit maps

☐ Window bars/locks

☐ Safe

☐ Wi-Fi/Internet access
