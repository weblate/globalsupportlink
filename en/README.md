---
description: Powered by the people and ready to assist!
---

# Welcome to your help desk

The Help Desk provides free resources to people and organizations who are part of the PxP global network. In addition to the service guide and online resources described below, members of the [Help Desk](ask-for-help.md) can answer questions and provide advice on digital and physical security via chat, messaging or email channels. Depending on your needs, the Help Desk can also organize:

* In-person workshops and trainings on security & resiliency for civil society organizations
* Digital security investigations
* Expert support on developing security policies
* Travel security briefings
* Psychosocial support

Reach out today to discuss with a member of the [Help Desk](ask-for-help.md) to see what is right for you or your organization!

The service guide covers three areas:

* [Digital Security](digital/)
  * Includes security and privacy guides for both individuals, and organizations
* [Physical Security](physical/)
  * Includes information on preparing for and during travel and other situations
* [Psychosocial Support](psychosocial.md)
  * Sharing access to opportunities and resources from community partner organizations

This is an evolving resource that will be updated and improved with feedback from all of you.

If you need more help, that what is provided above you can [Ask for Help!](ask-for-help.md)
