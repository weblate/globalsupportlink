# 👯 About Us

**TibCERT (**[**tibcert.org**](https://tibcert.org)**)** \
The Tibetan Computer Emergency Readiness Team (TibCERT) was formed to address the growing cybersecurity threats faced by Tibetan activists, journalists, and CSOs operating in politically sensitive environments. Recognizing the critical need to enhance digital security awareness and resilience, TibCERT aims to empower individuals and organizations throughout the world with the knowledge, tools, and resources necessary to defend against cyber threats effectively.

**Keri Caring for Activists (**[**facebook.com/KeriMentalHealth/**](https://facebook.com/KeriMentalHealth/)**)**\
A collective of activists supporting the mental health and wellbeing of social justice defenders. They provide social justice and indigenous/critical psychology-informed mental health and psychosocial support to activists at risk, while practicing trauma-informed care and LGBTQ+ affirmative counseling. They also provide psychosocial security consultancies and collective care workshops to movements and rights organizations.

**Videre (**[**videreonline.org**](https://videreonline.org)**)**\
Videre’s methodology overcomes a fundamental problem with traditional human rights documentation: its failure to centre and elevate frontline voices and knowledge. Many communities face significant security challenges when gathering the details of the violations they suffer, and verification and dissemination problems stymie their effectiveness and utility. We overcome this by developing close, enduring relationships with local activists and communities, reaching areas where the most vulnerable live and harnessing the power of local knowledge

**Guardian Project (**[**guardianproject.info**](https://guardianproject.info)**)**

With 15 years of experience in the Internet Freedom space, Guardian Project works to give all people a voice, enhance safety, and provide access to knowledge, regardless of location or connectivity. We build our work together upon free and open-source software, end-to-end encryption, ethical design processes, code and accessibility audits, collaborative localization, and other mutually beneficial approaches to delivering technology-based solutions.

\
