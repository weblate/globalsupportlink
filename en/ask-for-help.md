---
description: All the ways you can reach out to ask for help
---

# ☎️ Ask for Help!

This service is still being fully rolled out. However, we currently have two ways that you can reach out for help, to ask a question, report an incident, or others get connected with the help desk team.

Currently, we can provide advice and consulation on the topics related to digital and physical security, planning, and resilience.

* Public help desk chat: [chat.globalsupport.link](https://chat.globalsupport.link)
*   Help desk support

    * email: [help@globalsupport.link](mailto:help@globalsupport.link)
    * whatsapp: [+447886176827](https://wa.me/+447886176827)
    * telegram: [@GlobalSupportLink\_bot](https://t.me/GlobalSupportLink\_bot)



_This help desk is powered by_ [_free, open-source, secure and audited software_](https://digiresilience.org/solutions/link/)_, and hosted in a privacy-focused, environmentally concerned, and physically controlled_ [_datacenter_](https://greenhost.net/)_._
